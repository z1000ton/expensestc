package com.kekoroszlan.ml044.data.repositoryimpl

import com.kekoroszlan.ml044.data.room.dao.ExpensesDao
import com.kekoroszlan.ml044.domain.entity.ExpenseEntity
import com.kekoroszlan.ml044.domain.entity.ExpenseListEntity
import com.kekoroszlan.ml044.domain.repository.LocalRepository
import javax.inject.Inject

class LocalRepositoryImpl @Inject constructor(private val expensesDao: ExpensesDao) : LocalRepository {

    override suspend fun saveExpense(expense: ExpenseEntity) {
       expensesDao.insertExpenses(expense)
    }

    override suspend fun getExpenseList(): ExpenseListEntity {
        return ExpenseListEntity(expensesDao.getAll())
    }
}