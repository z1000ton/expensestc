package com.kekoroszlan.ml044.data.room.database

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kekoroszlan.ml044.data.room.dao.ExpensesDao
import com.kekoroszlan.ml044.domain.entity.ExpenseEntity

@Database(
    entities = [ExpenseEntity::class],
    version = 1
)
abstract class ExpensesDatabase : RoomDatabase() {
    abstract fun getDao(): ExpensesDao
}