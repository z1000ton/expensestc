package com.kekoroszlan.ml044.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kekoroszlan.ml044.domain.entity.ExpenseEntity


@Dao
interface ExpensesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertExpenses(vararg expenses: ExpenseEntity)

    @Query("SELECT * FROM expenses")
    fun getAll(): List<ExpenseEntity>

    @Insert
    fun insertAll(expenseEntity: ExpenseEntity)

    @Query("DELETE FROM expenses")
    fun deleteAll()
}
