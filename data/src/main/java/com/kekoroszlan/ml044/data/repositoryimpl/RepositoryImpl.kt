package com.kekoroszlan.ml044.data.repositoryimpl

import com.kekoroszlan.ml044.domain.entity.ExpenseEntity
import com.kekoroszlan.ml044.domain.repository.LocalRepository
import com.kekoroszlan.ml044.domain.repository.NetRepository
import com.kekoroszlan.ml044.domain.repository.Repository
import javax.inject.Inject

class  RepositoryImpl @Inject constructor(
    private val netRepository: NetRepository,
    private val localRepository: LocalRepository
) : Repository {

    override suspend fun saveExpense(expense: ExpenseEntity) {
        localRepository.saveExpense(expense)
    }

    override suspend fun getExpenseList() = localRepository.getExpenseList()
}