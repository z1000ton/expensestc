package com.kekoroszlan.ml044.data.repositoryimpl

import com.kekoroszlan.ml044.domain.entity.ExpenseEntity
import com.kekoroszlan.ml044.domain.entity.ExpenseListEntity
import com.kekoroszlan.ml044.domain.repository.NetRepository
import javax.inject.Inject

class NetRepositoryImpl @Inject constructor() : NetRepository {
    private val list = ArrayList<ExpenseEntity>()

    override suspend fun saveExpense(expense: ExpenseEntity) {
        list.add(expense)
    }

    override suspend fun getExpenseList(): ExpenseListEntity {
        if (list.isNotEmpty()) return ExpenseListEntity(list)
        for (i in 0..3) {
            val expense = ExpenseEntity(
                (2 * i).toDouble(),
                System.currentTimeMillis(),
                "Net $i"
            )
            list.add(expense)
        }
        return ExpenseListEntity(list)
    }
}