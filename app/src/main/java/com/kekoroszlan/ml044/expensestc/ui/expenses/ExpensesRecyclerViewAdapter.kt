package com.kekoroszlan.ml044.expensestc.ui.expenses


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kekoroszlan.ml044.domain.model.ExpenseModel
import com.kekoroszlan.ml044.domain.util.DateUtils
import com.kekoroszlan.ml044.expensestc.R
import kotlinx.android.synthetic.main.row_expenses.view.*


class ExpensesRecyclerViewAdapter(
    private val mContext: Context,
    private val mValues: ArrayList<ExpenseModel>
) : RecyclerView.Adapter<ExpensesRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_expenses, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = mValues.size

    fun updateData(values: List<ExpenseModel>) {
        mValues.clear()
        mValues.addAll(values)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {
        private val mTextDate: TextView = mView.row_expenses_text_date
        private val mTextConcept: TextView = mView.row_expenses_text_concept
        private val mTextAmount: TextView = mView.row_expenses_text_amount

        fun bind(item: ExpenseModel) {
            mTextDate.text =
                DateUtils.epochToString(item.timestamp, DateUtils.FORMAT_DATE_DEFAULT_NO_YEAR)
            mTextConcept.text = item.concept
            mTextAmount.text = String.format(
                mContext.getString(R.string.row_expense_msg_amount),
                item.amount,
                "€"
            )
        }
    }
}
