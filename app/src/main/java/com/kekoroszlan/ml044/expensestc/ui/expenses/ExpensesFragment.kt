package com.kekoroszlan.ml044.expensestc.ui.expenses

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kekoroszlan.ml044.domain.model.ExpenseModel
import com.kekoroszlan.ml044.expensestc.R
import com.kekoroszlan.ml044.expensestc.di.Injectable
import com.kekoroszlan.ml044.expensestc.ui.common.BaseFragment
import com.kekoroszlan.ml044.expensestc.util.ext.getFragmentViewModel
import com.kekoroszlan.ml044.expensestc.util.ext.observe
import kotlinx.android.synthetic.main.expenses_fragment.*
import javax.inject.Inject


class ExpensesFragment : BaseFragment(), Injectable {
    private var listener: OnFragmentInteractionListener? = null
    private var mRecyclerExpenses: RecyclerView? = null
    private var mAdapterExpenses: ExpensesRecyclerViewAdapter? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { getFragmentViewModel<ExpensesViewModel>(viewModelFactory) }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.expenses_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mAdapterExpenses = ExpensesRecyclerViewAdapter(activity as Context, ArrayList())
        mRecyclerExpenses = expenses_recycler_expenses

        mRecyclerExpenses?.let {
            it.layoutManager = LinearLayoutManager(activity)
            it.adapter = mAdapterExpenses
        }
        initObservables()
        initListeners()
        viewModel.getExpenses()
    }

    private fun initListeners() {
        expenses_button_create.setOnClickListener {
            listener?.onCreateNewExpense()
        }
    }

    override fun initObservables() {
        observe(viewModel.expenses) { expenses -> expenses?.let { onExpensesLoaded(expenses) } }
    }

    private fun onExpensesLoaded(expenses: List<ExpenseModel>) {
        mAdapterExpenses?.updateData(expenses)
    }

    fun updateExpenses() {
        viewModel.getExpenses()
    }


    interface OnFragmentInteractionListener {
        fun onCreateNewExpense()
    }

    companion object {
        fun newInstance() = ExpensesFragment()
    }
}
