package com.kekoroszlan.ml044.expensestc.di.modules

import com.kekoroszlan.ml044.expensestc.ui.addexpense.AddExpenseFragment
import com.kekoroszlan.ml044.expensestc.ui.expenses.ExpensesActivity
import com.kekoroszlan.ml044.expensestc.ui.expenses.ExpensesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentActivityModule {
    //Activities
    @ContributesAndroidInjector
    internal abstract fun contributeExpensesInjector(): ExpensesActivity

    //Fragments
    @ContributesAndroidInjector
    abstract fun contributeExpensesFragment(): ExpensesFragment

    @ContributesAndroidInjector
    abstract fun contributeAddExpenseFragment(): AddExpenseFragment
}