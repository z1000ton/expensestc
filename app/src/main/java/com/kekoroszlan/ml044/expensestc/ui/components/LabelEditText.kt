package com.kekoroszlan.ml044.expensestc.ui.components

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.kekoroszlan.ml044.expensestc.R
import kotlinx.android.synthetic.main.custom_label_edit_text.view.*

open class LabelEditText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.custom_label_edit_text, this, true)

        attrs?.let {
            val typedArray = context.obtainStyledAttributes(it, R.styleable.LabelEditText, 0, 0)
            val label = typedArray.getString(R.styleable.LabelEditText_label)
            val text = typedArray.getString(R.styleable.LabelEditText_text)

            label?.let { label_edit_text_label.text = label }
            text?.let { label_edit_text_content.setText(text) }
            typedArray.recycle()
        }
    }

    fun setLabel(label: String) {
        this.label_edit_text_label.text = label
    }

    fun setText(text: String) {
        this.label_edit_text_content.setText(text)
    }

    val text: String
        get() = label_edit_text_content.text.toString()
}