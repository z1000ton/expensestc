package com.kekoroszlan.ml044.expensestc.ui.components

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.util.AttributeSet
import com.kekoroszlan.ml044.domain.util.DateUtils
import com.kekoroszlan.ml044.domain.util.getInt
import kotlinx.android.synthetic.main.custom_label_edit_text.view.*

@SuppressLint("SetTextI18n")
class DateLabelEditText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LabelEditText(context, attrs, defStyleAttr) {

    var timestamp = 0L
    var picker: DatePickerDialog? = null

    init {
        this.label_edit_text_content.isFocusableInTouchMode = false

        this.label_edit_text_content.setOnClickListener {
            if (picker?.isShowing == true) return@setOnClickListener

            val currentMillis = if (timestamp == 0L) System.currentTimeMillis() else timestamp

            val date = DateUtils.epochToString(currentMillis, DateUtils.FORMAT_DATE_DEFAULT)

            date?.let {
                val dateArray = date.split("/")
                picker = DatePickerDialog(
                    context,
                    OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        label_edit_text_content.setText("$dayOfMonth/${monthOfYear + 1}")

                        timestamp = DateUtils.stringToEpoch(
                            "$dayOfMonth/${monthOfYear + 1}/$year",
                            DateUtils.FORMAT_DATE_DEFAULT
                        )
                    },
                    dateArray[2].getInt(),
                    dateArray[1].getInt() - 1,
                    dateArray[0].getInt()
                )
                picker?.show()
            }
        }
    }
}