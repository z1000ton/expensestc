package com.kekoroszlan.ml044.expensestc.di.modules

import com.kekoroszlan.ml044.domain.repository.Repository
import com.kekoroszlan.ml044.domain.usecase.GetExpenseListUseCase
import com.kekoroszlan.ml044.domain.usecase.SaveExpenseUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {
    @Provides
    fun getExpenses(repository: Repository): GetExpenseListUseCase =
        GetExpenseListUseCase(repository)

    @Provides
    fun getSaveExpense(repository: Repository): SaveExpenseUseCase =
        SaveExpenseUseCase(repository)
}