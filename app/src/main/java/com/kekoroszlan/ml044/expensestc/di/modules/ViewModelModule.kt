package com.kekoroszlan.ml044.expensestc.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kekoroszlan.ml044.expensestc.di.ViewModelFactory
import com.kekoroszlan.ml044.expensestc.di.ViewModelKey
import com.kekoroszlan.ml044.expensestc.ui.addexpense.AddExpenseViewModel
import com.kekoroszlan.ml044.expensestc.ui.expenses.ExpensesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ExpensesViewModel::class)
    abstract fun provideExpensesViewModel(expensesViewModel: ExpensesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddExpenseViewModel::class)
    abstract fun provideAddExpenseViewModel(addExpenseViewModel: AddExpenseViewModel): ViewModel
}