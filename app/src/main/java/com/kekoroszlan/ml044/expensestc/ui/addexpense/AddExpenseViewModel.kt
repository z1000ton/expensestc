package com.kekoroszlan.ml044.expensestc.ui.addexpense

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kekoroszlan.ml044.domain.model.ExpenseModel
import com.kekoroszlan.ml044.domain.usecase.SaveExpenseUseCase
import com.kekoroszlan.ml044.domain.util.NetworkViewState
import com.kekoroszlan.ml044.expensestc.ui.common.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AddExpenseViewModel @Inject constructor(
    private val saveExpenseUseCase: SaveExpenseUseCase
) :
    BaseViewModel() {

    private var _validData = MutableLiveData<Boolean>()
    val validData: LiveData<Boolean>
        get() = _validData

    fun saveNewExpense(expense: ExpenseModel) {
        if (!expense.isValidData()) {
            _validData.value = false
            return
        }

        launch {
            val data = withContext(Dispatchers.IO) {
                saveExpenseUseCase.execute(saveExpenseUseCase.Params(expense))
            }
            handleSaveExpenseResp(data)
        }
    }

    private fun handleSaveExpenseResp(state: NetworkViewState<ExpenseModel>) {
        when (state) {
            is NetworkViewState.Loading -> {
            }
            is NetworkViewState.Success -> {
                _validData.value = true
            }
            is NetworkViewState.Error -> {
            }
        }
    }

}

