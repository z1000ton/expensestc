package com.kekoroszlan.ml044.expensestc.ui.expenses

import android.os.Bundle
import com.kekoroszlan.ml044.expensestc.R
import com.kekoroszlan.ml044.expensestc.ui.addexpense.AddExpenseFragment
import com.kekoroszlan.ml044.expensestc.ui.common.BaseActivity
import com.kekoroszlan.ml044.expensestc.util.ext.invisible
import com.kekoroszlan.ml044.expensestc.util.ext.setContentFragment
import com.kekoroszlan.ml044.expensestc.util.ext.visible
import kotlinx.android.synthetic.main.expenses_activity.*

class ExpensesActivity : BaseActivity(), ExpensesFragment.OnFragmentInteractionListener,
    AddExpenseFragment.OnFragmentInteractionListener {
    lateinit var expenseListFragment: ExpensesFragment
    lateinit var addExpenseFragment: AddExpenseFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.expenses_activity)
        expenseListFragment = ExpensesFragment.newInstance()
        addExpenseFragment = AddExpenseFragment.newInstance()
        setContentFragment(R.id.expenses_list_container) { expenseListFragment }
        setContentFragment(R.id.add_expense_form_container) { addExpenseFragment }
    }

    /**
     *ExpensesFragment.OnFragmentInteractionListener
     */
    override fun onCreateNewExpense() {
        add_expense_form_container.visible()
        expenses_scroll_container.scrollTo(0, add_expense_form_container.top)
    }

    /**
     * AddExpenseFragment.OnFragmentInteractionListener implementation
     */
    override fun onSaved() {
        expenseListFragment.updateExpenses()
        expenses_scroll_container.scrollTo(0, expenses_list_container.top)
    }

    override fun onCancel() {
        add_expense_form_container.invisible()
        expenses_scroll_container.scrollTo(0, expenses_list_container.top)
    }
}

