package com.kekoroszlan.ml044.expensestc.di.modules

import android.content.Context
import android.content.SharedPreferences
import com.kekoroszlan.ml044.expensestc.ExpensesApplication
import com.kekoroszlan.ml044.expensestc.R
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [FragmentActivityModule::class, ViewModelModule::class, UseCaseModule::class, RepositoryModule::class])
class ApplicationModule(val app: ExpensesApplication) {
    @Provides
    fun provideApplication(): ExpensesApplication = app

    @Provides
    fun provideApplicationContext(): Context = app.applicationContext

    @Provides
    @Singleton
    internal fun provideSharedPreferences(): SharedPreferences {
        return app.getSharedPreferences(app.getString(R.string.app_name), 0)
    }
}
