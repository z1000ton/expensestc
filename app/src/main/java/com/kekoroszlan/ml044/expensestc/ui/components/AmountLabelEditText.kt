package com.kekoroszlan.ml044.expensestc.ui.components

import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import com.kekoroszlan.ml044.domain.util.getLong
import kotlinx.android.synthetic.main.custom_label_edit_text.view.*


class AmountLabelEditText @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LabelEditText(context, attrs, defStyleAttr) {

    init {
        this.label_edit_text_content.inputType =
            InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
    }

    val amount: Long
        get() = this.label_edit_text_content.text.toString().getLong()
}