package com.kekoroszlan.ml044.expensestc.ui.common

import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    abstract fun initObservables()
}
