package com.kekoroszlan.ml044.expensestc.di.components

import com.kekoroszlan.ml044.expensestc.ExpensesApplication
import com.kekoroszlan.ml044.expensestc.di.modules.ApplicationModule
import com.kekoroszlan.ml044.expensestc.di.modules.RoomModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, ApplicationModule::class, RoomModule::class])
interface ApplicationComponent : AndroidInjector<ExpensesApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: ExpensesApplication): Builder

        fun appModule(appModule: ApplicationModule): Builder

        fun roomModule(roomModule: RoomModule): Builder

        fun build(): ApplicationComponent
    }
}
