package com.kekoroszlan.ml044.expensestc.di.modules

import com.kekoroszlan.ml044.data.repositoryimpl.LocalRepositoryImpl
import com.kekoroszlan.ml044.data.repositoryimpl.NetRepositoryImpl
import com.kekoroszlan.ml044.data.repositoryimpl.RepositoryImpl
import com.kekoroszlan.ml044.domain.repository.LocalRepository
import com.kekoroszlan.ml044.domain.repository.NetRepository
import com.kekoroszlan.ml044.domain.repository.Repository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {
    @Singleton
    @Binds
    abstract fun provideNetRepository(netRepository: NetRepositoryImpl): NetRepository

    @Singleton
    @Binds
    abstract fun provideLocalRepository(preferenceRepositoryImpl: LocalRepositoryImpl)
            : LocalRepository

    @Singleton
    @Binds
    abstract fun provideRepository(repository: RepositoryImpl): Repository
}