package com.kekoroszlan.ml044.expensestc.util.ext

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes

fun View.isVisible() = this.visibility == View.VISIBLE

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.selectVisibility(shouldBeVisible: Boolean) {
    this.visibility = if (shouldBeVisible) View.VISIBLE else View.GONE
}

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Context.showToast(@StringRes resId: Int, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, this.getString(resId), duration).show()
}

