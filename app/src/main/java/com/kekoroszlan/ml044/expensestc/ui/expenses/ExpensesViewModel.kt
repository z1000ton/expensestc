package com.kekoroszlan.ml044.expensestc.ui.expenses

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kekoroszlan.ml044.domain.model.ExpenseModel
import com.kekoroszlan.ml044.domain.usecase.GetExpenseListUseCase
import com.kekoroszlan.ml044.domain.util.NetworkViewState
import com.kekoroszlan.ml044.expensestc.ui.common.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ExpensesViewModel @Inject constructor(
    private val getExpenseListUseCase: GetExpenseListUseCase
) :
    BaseViewModel() {

    private var _expenses = MutableLiveData<List<ExpenseModel>>()
    val expenses: LiveData<List<ExpenseModel>>
        get() = _expenses

    fun getExpenses() {
        launch {
            val data = withContext(Dispatchers.IO) {
                getExpenseListUseCase.execute(getExpenseListUseCase.Params())
            }
            handleExpensesResponse(data)
        }
    }

    private fun handleExpensesResponse(state: NetworkViewState<List<ExpenseModel>>) {
        when (state) {
            is NetworkViewState.Loading -> {
            }
            is NetworkViewState.Success -> {
                _expenses.value = state.data
            }
            is NetworkViewState.Error -> {
            }
        }
    }
}

