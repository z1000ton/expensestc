package com.kekoroszlan.ml044.expensestc

import com.kekoroszlan.ml044.expensestc.di.applyAutoInjector
import com.kekoroszlan.ml044.expensestc.di.components.DaggerApplicationComponent
import com.kekoroszlan.ml044.expensestc.di.modules.ApplicationModule
import com.kekoroszlan.ml044.expensestc.di.modules.RoomModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class ExpensesApplication : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        applyAutoInjector()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.builder()
            .application(this)
            .appModule(ApplicationModule(this))
            .roomModule(RoomModule(this))
            .build()
    }
}
