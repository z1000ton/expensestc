package com.kekoroszlan.ml044.expensestc.ui.addexpense

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.kekoroszlan.ml044.domain.model.ExpenseModel
import com.kekoroszlan.ml044.domain.util.getDouble
import com.kekoroszlan.ml044.expensestc.R
import com.kekoroszlan.ml044.expensestc.di.Injectable
import com.kekoroszlan.ml044.expensestc.ui.common.BaseActivity
import com.kekoroszlan.ml044.expensestc.ui.common.BaseFragment
import com.kekoroszlan.ml044.expensestc.util.ext.getFragmentViewModel
import com.kekoroszlan.ml044.expensestc.util.ext.observe
import com.kekoroszlan.ml044.expensestc.util.ext.showToast
import kotlinx.android.synthetic.main.fragment_add_expense.*
import javax.inject.Inject

class AddExpenseFragment : BaseFragment(), Injectable {
    private var listener: OnFragmentInteractionListener? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy { getFragmentViewModel<AddExpenseViewModel>(viewModelFactory) }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_expense, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initListeners()
        initObservables()
    }

    override fun initObservables() {
        observe(viewModel.validData) { valid ->
            if (valid == true) {
                cleanFields()
                listener?.onSaved()
                activity?.showToast(R.string.add_expense_msg_saved_successfully)
            } else {
                activity?.showToast(R.string.add_expense_error_empty_ot_worng_field)
            }

        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun initListeners() {
        add_expenses_button_save.setOnClickListener {
            val expense = ExpenseModel(
                add_expenses_label_edit_amount.text.getDouble(),
                add_expenses_label_edit_date.timestamp,
                add_expenses_label_edit_concept.text
            )
            viewModel.saveNewExpense(expense)
        }

        add_expenses_button_cancel.setOnClickListener {
            cleanFields()
            listener?.onCancel()
        }
    }

    private fun cleanFields() {
        add_expenses_label_edit_date.setText("")
        add_expenses_label_edit_date.timestamp = 0L
        add_expenses_label_edit_concept.setText("")
        add_expenses_label_edit_amount.setText("")

        (activity as BaseActivity).hideKeyboard()
    }

    interface OnFragmentInteractionListener {
        fun onSaved()
        fun onCancel()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            AddExpenseFragment()
    }
}
