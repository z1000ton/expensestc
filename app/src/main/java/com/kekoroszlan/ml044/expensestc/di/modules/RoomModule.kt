package com.kekoroszlan.ml044.expensestc.di.modules

import androidx.room.Room
import com.kekoroszlan.ml044.data.room.dao.ExpensesDao
import com.kekoroszlan.ml044.data.room.database.ExpensesDatabase
import com.kekoroszlan.ml044.expensestc.ExpensesApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule(val app: ExpensesApplication) {
    @Singleton
    @Provides
    fun getExpensesDataBase(): ExpensesDatabase {
        return Room.databaseBuilder(
            app,
            ExpensesDatabase::class.java, "expenses_database"
        ).fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun getExpensesDao(expensesDatabase: ExpensesDatabase): ExpensesDao {
        return expensesDatabase.getDao()
    }

}