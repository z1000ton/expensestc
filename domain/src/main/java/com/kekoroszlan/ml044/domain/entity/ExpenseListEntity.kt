package com.kekoroszlan.ml044.domain.entity

import com.google.gson.annotations.SerializedName

data class ExpenseListEntity(@SerializedName("expenses") val _expenses: List<ExpenseEntity>? = ArrayList()) {
    val expenses get() = _expenses ?: ArrayList()
}