package com.kekoroszlan.ml044.domain.util

sealed class NetworkViewState<out T> {
    object Loading : NetworkViewState<Nothing>()
    class Success<out T>(val data: T) : NetworkViewState<T>()
    class Error(var error: Any) : NetworkViewState<Nothing>()
}
