package com.kekoroszlan.ml044.domain.usecase

import com.kekoroszlan.ml044.domain.util.NetworkViewState

/**
 * @param T is the response data
 * @param PARAMS is the request data
 */
abstract class UseCase<T, PARAMS> protected constructor() {

    protected abstract suspend fun buildUseCase(params: PARAMS): NetworkViewState<T>

    suspend fun execute(params: PARAMS) = buildUseCase(params)
}

