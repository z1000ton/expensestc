package com.kekoroszlan.ml044.domain.util

import android.annotation.SuppressLint
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DateUtils {

    companion object {
        //ex: 04/05/1979
        val FORMAT_DATE_DEFAULT = "dd/MM/yyyy"

        //ex: 04/05/1979
        val FORMAT_DATE_DEFAULT_NO_YEAR = "dd/MM"

        // ex: 19790504
        val FORMAT_DATE_REVERSE_NO_SLASH = "yyyyMMdd"

        // ex: 1979-05-04
        private val FORMAT_DATE_REVERSE_HYPHEN = "yyyy-MM-dd"

        // ex: 12:59:59
        val FORMAT_TIME_COLON = "HH:mm:ss"


        fun epochToString(timeMilliseconds: Long, format: String): String? {
            val simpleDateFormat =
                SimpleDateFormat(format, Locale.getDefault())
            return simpleDateFormat.format(timeMilliseconds)
        }

        @SuppressLint("SimpleDateFormat")
        fun stringToEpoch(date: String, format: String): Long {
            val formatter: DateFormat = SimpleDateFormat(format)
            val date = formatter.parse(date) as Date
            return date.time
        }
    }

}