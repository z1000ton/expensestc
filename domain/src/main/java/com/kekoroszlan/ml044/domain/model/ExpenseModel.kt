package com.kekoroszlan.ml044.domain.model

import com.kekoroszlan.ml044.domain.entity.ExpenseEntity


class ExpenseModel(val amount: Double, val timestamp: Long, val concept: String) {
    fun isValidData(): Boolean {
        return !(amount <= 0 || timestamp <= 0L || concept.isEmpty())
    }
}

fun ExpenseModel.toEntity(): ExpenseEntity {
    return ExpenseEntity(this.amount, this.timestamp, this.concept)
}