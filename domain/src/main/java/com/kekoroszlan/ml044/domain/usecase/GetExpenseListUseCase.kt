package com.kekoroszlan.ml044.domain.usecase

import com.kekoroszlan.ml044.domain.entity.toModel
import com.kekoroszlan.ml044.domain.model.ExpenseModel
import com.kekoroszlan.ml044.domain.repository.Repository
import com.kekoroszlan.ml044.domain.util.NetworkViewState

class GetExpenseListUseCase(private val repository: Repository) :
    UseCase<List<ExpenseModel>, GetExpenseListUseCase.Params>() {

    override suspend fun buildUseCase(params: Params): NetworkViewState<List<ExpenseModel>> {
        return try {
            val expenseListEntity = repository.getExpenseList()
            val expenses =
                expenseListEntity.expenses.sortedByDescending { it.id }.map { it.toModel() }
                    .toList().sortedByDescending { it.timestamp }
            NetworkViewState.Success(expenses)
        } catch (throwable: Throwable) {
            NetworkViewState.Error(throwable)
        }
    }

    inner class Params
}
