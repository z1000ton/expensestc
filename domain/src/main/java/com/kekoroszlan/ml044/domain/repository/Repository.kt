package com.kekoroszlan.ml044.domain.repository

import com.kekoroszlan.ml044.domain.entity.ExpenseEntity
import com.kekoroszlan.ml044.domain.entity.ExpenseListEntity

interface Repository {
    suspend fun getExpenseList(): ExpenseListEntity
    suspend fun saveExpense(expense:ExpenseEntity)
}