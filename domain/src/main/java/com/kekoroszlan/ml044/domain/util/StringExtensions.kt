package com.kekoroszlan.ml044.domain.util

fun String.getInt(): Int {
    return try {
        this.toInt()
    } catch (e: Exception) {
        0
    }
}

fun String.getLong(): Long {
    return try {
        this.toLong()
    } catch (e: Exception) {
        0L
    }
}

fun String.getDouble(): Double {
    return try {
        this.toDouble()
    } catch (e: Exception) {
        0.0
    }
}
