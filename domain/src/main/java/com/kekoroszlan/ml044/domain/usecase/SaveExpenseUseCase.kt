package com.kekoroszlan.ml044.domain.usecase

import com.kekoroszlan.ml044.domain.model.ExpenseModel
import com.kekoroszlan.ml044.domain.model.toEntity
import com.kekoroszlan.ml044.domain.repository.Repository
import com.kekoroszlan.ml044.domain.util.NetworkViewState

class SaveExpenseUseCase(private val repository: Repository) :
    UseCase<ExpenseModel, SaveExpenseUseCase.Params>() {

    override suspend fun buildUseCase(params: Params): NetworkViewState<ExpenseModel> {
        return try {
            repository.saveExpense(params.expense.toEntity())
            NetworkViewState.Success(params.expense)
        } catch (throwable: Throwable) {
            NetworkViewState.Error(throwable)
        }
    }

    inner class Params(val expense: ExpenseModel)
}
