package com.kekoroszlan.ml044.domain.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.kekoroszlan.ml044.domain.model.ExpenseModel

@Entity(tableName = "expenses")

data class ExpenseEntity(
    @SerializedName("amount")
    @ColumnInfo(name = "amount")
    val amount: Double = 0.0,
    @SerializedName("timestamp")
    @ColumnInfo(name = "timestamp")
    val timestamp: Long = 0,
    @SerializedName("concept")
    @ColumnInfo(name = "concept")
    val _concept: String = ""
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}

fun ExpenseEntity.toModel() = ExpenseModel(this.amount, this.timestamp, this._concept)

